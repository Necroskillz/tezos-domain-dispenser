import { TezosToolkit } from '@taquito/taquito';
import dotenv from 'dotenv';
import { tezosToolkit } from '../common/tezos-toolkit';

export function run(what: (tezos: TezosToolkit) => Promise<void>) {
    dotenv.config();
    let tezos = tezosToolkit();
    what(tezos).catch(e => {
        debugger;
        console.error(e);
    });
}
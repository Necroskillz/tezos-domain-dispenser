import { TextEncoder } from 'util';

export function encodeString(str: string): string {
    var result = '';
    var encoded = new TextEncoder().encode(str);
    for (let i = 0; i < encoded.length; i++) {
        let hexchar = encoded[i].toString(16);
        result += hexchar.length == 2 ? hexchar : '0' + hexchar;
    }
    return result;
}

export function hexToArray(hexString: string): Uint8Array {
    return new Uint8Array(hexString.match(/.{1,2}/g)!.map(byte => parseInt(byte, 16)));
}

export function dateToTimestamp(date: Date): number {
    return Math.floor(date.getTime() / 1000);
}

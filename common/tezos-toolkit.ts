import { InMemorySigner } from '@taquito/signer';
import { TezosToolkit } from '@taquito/taquito';
import { Operation } from '@taquito/taquito/dist/types/operations/operations';
import { Tzip16Module } from '@taquito/tzip16';
import { currentProfile } from './profiles';

function delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export function tezosToolkit(): TezosToolkit {
    const profile = currentProfile();
    let rpc = process.env.RPC_URL || profile.rpc;
    let toolkit = new TezosToolkit(rpc);
    toolkit.setProvider({
        signer: new InMemorySigner(profile.secretKey),
        config: {
            confirmationPollingIntervalSecond: 1,
            confirmationPollingTimeoutSecond: 120,
        },
    });
    toolkit.addExtension(new Tzip16Module());
    return toolkit;
}

export async function runOperation<T extends Operation>(what: () => Promise<T>): Promise<T> {
    let op = await what();

    async function seenOperationInBlock(block?: string) {
        let fetchedBlock = await tezosToolkit().rpc.getBlock(block ? { block } : undefined);
        return !!fetchedBlock.operations.find(blockOps => !!blockOps.find(blockOp => blockOp.hash === op.hash));
    }

    // confirmation() has a known issue with missed confirmations.
    // see https://github.com/ecadlabs/taquito/issues/276

    // check previous block first
    if (await seenOperationInBlock('head~1')) {
        return op;
    }

    // keep looking
    const maxTries = 120;
    for (let i = 0; i < maxTries; i++) {
        if (await seenOperationInBlock()) {
            return op;
        }
        await delay(1000);
    }

    throw new Error(`Giving up after ${maxTries} tries waiting for a confirmation`);
}

import { runOperation, tezosToolkit } from '../tezos-toolkit';
import { currentProfile } from '../profiles';
import { compileContract } from '../compile';
import { MichelsonMap } from '@taquito/michelson-encoder';

export abstract class Contract {
    protected abstract get name(): string;

    async deploy(config: any = {}, innerStorage: any = {}) {
        const tezos = tezosToolkit();
        const owner = currentProfile().ownerAddress;
        const storage = {
            owner,
            ...(await this.buildInnerStorage(config)),
            ...innerStorage,
        };

        console.log(`Originating ${this.name}`);

        const op = await runOperation(
            async () =>
                await tezos.contract.originate({
                    code: await compileContract(`src/contracts/${this.name}.mligo`),
                    storage,
                    balance: config.balance,
                })
        );

        console.log(`Originated ${op.contractAddress}`);

        return await tezos.contract.at(op.contractAddress!);
    }

    protected abstract buildInnerStorage(config: any): Promise<any>;
}

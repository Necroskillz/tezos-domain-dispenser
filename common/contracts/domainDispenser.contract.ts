import { MichelsonMap } from '@taquito/michelson-encoder';
import { Contract } from './contract';
import configKeys from '../domainDispenser.configKeys';
import { encodeString } from '../convert';

export class DomainDispenserContract extends Contract {
    get name(): string {
        return 'DomainDispenser';
    }

    protected async buildInnerStorage(config: any): Promise<any> {
        const contractConfig = new MichelsonMap({ prim: 'map', args: [{ prim: 'nat' }, { prim: 'nat' }] });
        contractConfig.set(configKeys.cost, config.cost || 0);
        contractConfig.set(configKeys.maxLabelsPerAddress, config.maxLabelsPerAddress || 1);

        return {
            records: new MichelsonMap(),
            owners: new MichelsonMap(),
            reserved_labels: [],
            config: contractConfig,
            set_child_record_proxy: config.setChildRecordProxy,
            parent_domain: encodeString(config.parentDomain)
        };
    }
}

export const domainDispenserContract = new DomainDispenserContract();

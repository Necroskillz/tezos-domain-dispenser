import accounts from './sandbox-accounts';

const alice = accounts.alice;

function profiles(): Record<string, any> {
    return {
        default: {
            // used for local tests
            rpc: 'http://localhost:8732',
            ownerAddress: alice.pkh,
            secretKey: alice.sk,
        },
        florencenet: {
            rpc: 'https://florencenet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // testnet specific settings
            parentDomain: 'my-name4.flo',
            setChildRecordProxy: 'KT1UG9DUYqtCF7XUqjH4QNGF2naoh7D1mscR',
            // deployed: require('../deployed/florencenet.json'),
        },
        mainnet: {
            rpc: 'https://mainnet.smartpy.io',
            ownerAddress: process.env.OWNER_ADDRESS,
            secretKey: process.env.ADMIN_SIGN_KEY,

            // mainnet specific settings
            parentDomain: 'my-name.tez',
            setChildRecordProxy: 'KT1QHLk1EMUA8BPH3FvRUeUmbTspmAhb7kpd'
        },
    };
}

export function currentProfile(): any {
    const name = process.env.PROFILE || 'default';
    const profile = profiles()[name];
    if (!profile) {
        throw new Error(`No such profile: ${name}`);
    }
    return profile;
}

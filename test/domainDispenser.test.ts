import { Contract } from '@taquito/taquito';
import { assert } from 'chai';
import { domainDispenserContract } from '../common/contracts/domainDispenser.contract';

describe('DomainDispenser', () => {
    let contract: Contract;

    before(async () => {
        contract = await domainDispenserContract.deploy({
            parentDomain: 'alice.tez',
            setChildRecordProxy: 'KT1GBZmSxmnKJXGMdMLbugPfLyUPmuLSMwKS',
            cost: 10000,
            maxLabelsPerAddress: 2
        });
    });

    it('should deploy', () => {
        assert.isNotNull(contract);
    });
});
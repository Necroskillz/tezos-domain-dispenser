type record = {
    owner: address;
}

type storage = {
    records: (bytes, record) big_map;
    owners: (address, int) big_map;
    reserved_labels: bytes set;
    config: (nat, nat) map;
    set_child_record_proxy: address;
    parent_domain: bytes;
    owner: address;
}

type return = operation list * storage

type claim_param = {
    label: bytes;
}

type release_param = {
    label: bytes;
}

type admin_update_param = storage -> return

type parameter =
    Claim of claim_param
    | Release of release_param
    | Admin_update of admin_update_param

type data_map = (string, bytes) map

type set_child_record_param = [@layout:comb] {
    label: bytes;
    parent: bytes;
    address: address option;
    owner: address;
    data: data_map;
    expiry: timestamp option;
}

let cost_config_key = 0n
let max_labels_per_address_key = 1n

[@inline]
let get_config (key, store : nat * storage) : nat option =
    Map.find_opt key store.config

[@inline]
let require_config (key, store : nat * storage) : nat =
    match get_config (key, store) with
        Some v -> v
        | None -> (failwith "INTERNAL_CONFIG_MISSING" : nat)

let require_available_for_sender(label, store: bytes * storage) : unit =
    let assert_available = match Big_map.find_opt label store.records with
        Some r -> (failwith "LABEL_ALREADY_CLAIMED": unit)
        | None -> () in

    let max_domains: int = require_config (max_labels_per_address_key, store) + 0 in

    match Big_map.find_opt Tezos.sender store.owners with
        Some count -> if count >= max_domains then (failwith "ADDRESS_ALREADY_OWNS_MAX_AMOUNT_OF_DOMAINS": unit) else ()
        | None -> ()

let require_non_reserved_label(label, store: bytes * storage): unit =
    if Set.mem label store.reserved_labels then (failwith "LABEL_RESERVED": unit) else ()

let require_correct_amount(store: storage): unit =
    let price = require_config (cost_config_key, store) * 1mutez in
    if price <> Tezos.amount then (failwith "INCORRECT_AMOUNT": unit) else ()

let write_record(p, store: claim_param * storage): storage =
    let new_record: record = {
        owner = Tezos.sender
    } in
    let current_count = match Big_map.find_opt Tezos.sender store.owners with
        Some count -> count
        | None -> 0 in
    let next_count = current_count + 1 in
    { store with
        records = Big_map.update p.label (Some new_record) store.records;
        owners = Big_map.update Tezos.sender (Some next_count) store.owners;
    }

let remove_record(p, store: claim_param * storage): storage =
    let current_count = match Big_map.find_opt Tezos.sender store.owners with
        Some count -> count
        | None -> (failwith "INCONSISTENCY_ERROR": int) in
    let next_count = current_count - 1 in
    { store with
        records = Big_map.update p.label (None: record option) store.records;
        owners = Big_map.update Tezos.sender (Some next_count) store.owners;
    }

let admin_update (p, store : admin_update_param * storage) : return =
    // if valid owner, execute
    if Tezos.sender = store.owner then p store

    // fail for everyone else
    else (failwith "NOT_AUTHORIZED" : return)

let claim(p, store: claim_param * storage): return =
    let assert_available = require_available_for_sender(p.label, store) in
    let assert_non_reserved_label = require_non_reserved_label(p.label, store) in
    let assert_amount = require_correct_amount(store) in
    let new_store = write_record(p, store) in

    match (Tezos.get_entrypoint_opt "%set_child_record" store.set_child_record_proxy : set_child_record_param contract option) with
        None -> (failwith "INVALID_SET_CHILD_RECORD_PROXY_ADDRESS": return)
        | Some c ->
            let set_child_record_param: set_child_record_param = {
                address = (None: address option);
                expiry = (None: timestamp option);
                label = p.label;
                parent = store.parent_domain;
                owner = Tezos.sender;
                data = (Map.empty: data_map);
            } in
            [Tezos.transaction set_child_record_param 0mutez c], new_store

let release(p, store: release_param * storage): return =
    let record = match Big_map.find_opt p.label store.records with
        Some r -> r
        | None -> (failwith "LABEL_NOT_CLAIMED": record) in
    
    let assert_owner = if record.owner <> Tezos.sender then (failwith "NOT_AUTHORIZED": unit) else () in

    let new_store = remove_record(p, store) in

    match (Tezos.get_entrypoint_opt "%set_child_record" store.set_child_record_proxy : set_child_record_param contract option) with
        None -> (failwith "INVALID_SET_CHILD_RECORD_PROXY_ADDRESS": return)
        | Some c ->
            let set_child_record_param: set_child_record_param = {
                address = (None: address option);
                expiry = (None: timestamp option);
                label = p.label;
                parent = store.parent_domain;
                owner = store.owner;
                data = (Map.empty: data_map);
            } in
            [Tezos.transaction set_child_record_param 0mutez c], new_store

let main (action, store: parameter * storage): return =
    match action with
        | Claim p -> claim(p, store)
        | Release p -> release(p, store)
        | Admin_update p -> admin_update(p, store)

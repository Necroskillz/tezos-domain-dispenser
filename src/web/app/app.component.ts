import { AccountInfo, NetworkType, PermissionScope } from '@airgap/beacon-sdk';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BeaconWallet } from '@taquito/beacon-wallet';
import { TezosToolkit } from '@taquito/taquito';

export function encodeString(str: string): string {
    let result = '';
    const encoded = new TextEncoder().encode(str);
    for (let i = 0; i < encoded.length; i++) {
        const hexchar = encoded[i].toString(16);
        result += hexchar.length === 2 ? hexchar : '0' + hexchar;
    }
    return result;
}

export interface Config {
    rpcUrl: string;
    network: string;
    parentDomain: string;
    contractAddress: string;
    appUrl: string;
}

const TestConfig: Config = {
    contractAddress: 'KT1MLdJ3uKK72KNutHWWryv8ZjPscj4LoRz9',
    network: 'florencenet',
    parentDomain: 'my-name4.flo',
    rpcUrl: 'https://florencenet.smartpy.io',
    appUrl: 'https://staging-app.tezos.domains',
};

const MainnetConfig: Config = {
    contractAddress: '',
    network: 'mainnet',
    parentDomain: 'my-name.tez',
    rpcUrl: 'https://mainnet.smartpy.io',
    appUrl: 'https://app.tezos.domains',
};

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    title = 'app';
    wallet!: BeaconWallet;
    account?: AccountInfo;
    tezos!: TezosToolkit;
    form!: FormGroup;
    state?: 'info' | 'error' | 'success';
    message?: string;
    config = TestConfig;

    constructor(private formBuilder: FormBuilder) {}

    async ngOnInit(): Promise<void> {
        this.wallet = new BeaconWallet({
            name: 'Domain Dispenser',
        });

        this.tezos = new TezosToolkit(this.config.rpcUrl);
        this.tezos.setProvider({
            wallet: this.wallet,
        });

        this.form = this.formBuilder.group({
            label: this.formBuilder.control('', [Validators.required]),
        });

        this.account = await this.wallet.client.getActiveAccount();
    }

    async connect(): Promise<void> {
        this.resetState();

        await this.wallet.requestPermissions({
            network: { type: this.config.network as NetworkType },
            scopes: [PermissionScope.OPERATION_REQUEST, PermissionScope.SIGN],
        });

        this.account = await this.wallet.client.getActiveAccount();
    }

    async disconnect(): Promise<void> {
        await this.wallet.clearActiveAccount();
        this.account = undefined;
        this.resetState();
    }

    async claim(): Promise<void> {
        const label = this.form.value.label;

        this.execute(
            'claim',
            `You have successfully claimed the domain <a href="${this.config.appUrl}/domain/${label}.${this.config.parentDomain}" target="_blank">${label}.${this.config.parentDomain}</a>`
        );
    }

    async release(): Promise<void> {
        this.execute('release', `You have successfully released the domain and can claim another one.`);
    }

    private resetState(): void {
        this.state = undefined;
        this.message = undefined;
    }

    private async execute(endpoint: string, successMessage: string): Promise<void> {
        this.resetState();
        const label = this.form.value.label;

        try {
            this.form.disable();

            const contract = await this.tezos.wallet.at(this.config.contractAddress);
            const op = await contract.methods[endpoint](encodeString(label)).send();

            this.state = 'info';
            this.message = 'Confirming...';

            await op.confirmation();

            this.state = 'success';
            this.message = successMessage;
        } catch (err) {
            this.state = 'error';
            this.message = err.message;
        }

        this.form.enable();
        this.form.reset();
    }
}

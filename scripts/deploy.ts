
import { run } from '../common/utils';
import { domainDispenserContract } from '../common/contracts/domainDispenser.contract';
import { currentProfile } from '../common/profiles';

run(async tezos => {
    const profile = currentProfile();

    await domainDispenserContract.deploy({
        setChildRecordProxy: profile.setChildRecordProxy,
        parentDomain: profile.parentDomain
    });
});